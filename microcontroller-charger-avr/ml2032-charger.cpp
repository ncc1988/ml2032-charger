/*
 *  ml2032-charger
 *  Copyright (C) 2021-2022  Moritz Strohm <ncc1988@posteo.de>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "yauclib/Microcontroller.h"


constexpr uint16_t max_temp = (35 / 50) * 1024; //Raw pin value of the maximum temperature treshold.
constexpr uint16_t min_temp = 0;                //Raw pin value of the minimum temperature treshold.
constexpr uint16_t charge_end_voltage = (31 / 50) * 1024; //Raw pin value of the charge end voltage.
constexpr uint16_t overvoltage_treshold = (32 / 50) * 1024; //Raw pin value of the overvoltage treshold.
constexpr uint16_t deep_discharge_treshold = (20 / 50) * 1024; //Raw pin value of the deep discharge treshold.


//AVR ATTiny45 pin allocation:
//PB0: analog input 0: battery voltage
//PB1: analog input 1: temperature sensor
//PB2: digital output: Status LED
//PB3: digital output: Charging current
//PB4: digital output: watchdog signal
constexpr uint8_t LED_PIN = 7;
constexpr uint8_t HEARTBEAT_PIN = 3;
constexpr uint8_t CHARGING_PIN = 2;
constexpr uint8_t TEMPERATURE_PIN = 6;
constexpr uint8_t VOLTAGE_PIN = 5;


/**
 * Reads the temperature from PB1.
 */
bool temperatureOk()
{
    //Read the analog temperature value:
    uint16_t temp_raw = Microcontroller::readAnalogPin(TEMPERATURE_PIN);

    if ((temp_raw > max_temp) || (temp_raw < min_temp)) {
        return false;
    }
    return true;
}


/**
 * Checks the voltage.
 *
 * @returns int8_t A status for the measured voltage:
 *     -1 in case the voltage is below the deep discharge treshold.
 *     0 in case the voltage is in the acceptable range.
 *     1 in case the voltage has reached the charge end voltage.
 *     2 in case the voltage is over the overvoltage treshold.
 */
int8_t voltageOk()
{
    uint16_t voltage_raw = Microcontroller::readAnalogPin(VOLTAGE_PIN);

    if (voltage_raw > charge_end_voltage) {
        //Charge end voltage reached.
        return 1;
    }
    if (voltage_raw > overvoltage_treshold) {
        //Overvoltage treshold reached.
        return 2;
    }
    if (voltage_raw < deep_discharge_treshold) {
        //Deep discharge treshold reached.
        return -1;
    }
    //The voltage is in the acceptable range.
    return 0;
}


/**
 * Starts the charging process by setting PB3 to 1.
 */
void startCharging()
{
    Microcontroller::writePin(CHARGING_PIN, true);
    //Also enable the charging LED:
    Microcontroller::writePin(LED_PIN, true);
}


/**
 * Stops the charging process by setting PB2 to 0.
 */
void stopCharging()
{
    Microcontroller::writePin(CHARGING_PIN, false);
    //Also disable the charging LED:
    Microcontroller::writePin(LED_PIN, false);
}


/**
 * Halts the operation and blinks the charging LED.
 */
void haltTemperature()
{
    stopCharging();

    while (true) {
        Microcontroller::writePin(LED_PIN, true);
        Microcontroller::wait(125);
        Microcontroller::writePin(LED_PIN, false);
        Microcontroller::wait(125);
    }
}


void haltOvervoltage()
{
    stopCharging();

    while (true) {
        Microcontroller::writePin(LED_PIN, true);
        Microcontroller::wait(125);
        Microcontroller::writePin(LED_PIN, false);
        Microcontroller::wait(125);
        Microcontroller::writePin(LED_PIN, true);
        Microcontroller::wait(125);
        Microcontroller::writePin(LED_PIN, false);
        Microcontroller::wait(375);
   }
}


void haltUndervoltage()
{
    stopCharging();

    while (true) {
        Microcontroller::writePin(LED_PIN, true);
        Microcontroller::wait(125);
        Microcontroller::writePin(LED_PIN, false);
        Microcontroller::wait(750);
    }
}


/**
 * Halts the operation due to normal end of charging.
 */
void halt()
{
    stopCharging();

    //Turn off everything:
    Microcontroller::powerOff();
}


void handleVoltageStatus(int8_t voltage_status)
{
    if (voltage_status > 1) {
        haltOvervoltage();
    }
    if (voltage_status > 0) {
        halt();
    }
    if (voltage_status < 0) {
        haltUndervoltage();
    }
}


void sendHeartbeat()
{
    Microcontroller::writePin(HEARTBEAT_PIN, true);
    Microcontroller::wait(10);
    Microcontroller::writePin(HEARTBEAT_PIN, false);
    Microcontroller::wait(10);
}


/**
 * Sets up the pins.
 */
void setup()
{
    Microcontroller::setPinDirection(CHARGING_PIN, true);
    Microcontroller::setPinDirection(LED_PIN, true);
    Microcontroller::setPinDirection(HEARTBEAT_PIN, true);

    //Setup the analog inputs:
    Microcontroller::setupAnalogReading();
}


int main()
{
    setup();

    //Enable the LED for half a second, then wait half a second:
    Microcontroller::writePin(LED_PIN, true);
    Microcontroller::wait(500);
    Microcontroller::writePin(LED_PIN, false);
    Microcontroller::wait(500);

    bool temperature_ok = temperatureOk();
    if (!temperature_ok) {
        haltTemperature();
    }
    int8_t voltage_status = voltageOk();
    handleVoltageStatus(voltage_status);

    //All checks completed. We can start charging the battery:
    startCharging();
    do {
        Microcontroller::wait(50);
        voltage_status = voltageOk();
        temperature_ok = temperatureOk();
        sendHeartbeat();
    } while  (temperature_ok && voltage_status == 0);
    if (!temperature_ok) {
        haltTemperature();
    }
    handleVoltageStatus(voltage_status);
}
