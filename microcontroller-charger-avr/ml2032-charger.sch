EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "ML2032 charger"
Date "2021-09-30"
Rev ""
Comp "Moritz Strohm"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:Battery_Cell BT1
U 1 1 614C4370
P 10550 2650
F 0 "BT1" H 10668 2746 50  0000 L CNN
F 1 "ML2032" H 10668 2655 50  0000 L CNN
F 2 "" V 10550 2710 50  0001 C CNN
F 3 "~" V 10550 2710 50  0001 C CNN
	1    10550 2650
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LM317_3PinPackage U1
U 1 1 614C4579
P 1650 900
F 0 "U1" H 1650 1142 50  0000 C CNN
F 1 "LM317_3PinPackage" H 1650 1051 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-220-3_Vertical" H 1650 1150 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/lm317.pdf" H 1650 900 50  0001 C CNN
	1    1650 900 
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0101
U 1 1 614C4922
P 800 750
F 0 "#PWR0101" H 800 600 50  0001 C CNN
F 1 "VCC" H 817 923 50  0000 C CNN
F 2 "" H 800 750 50  0001 C CNN
F 3 "" H 800 750 50  0001 C CNN
	1    800  750 
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 614C49EA
P 800 6800
F 0 "#PWR0102" H 800 6550 50  0001 C CNN
F 1 "GND" H 805 6627 50  0000 C CNN
F 2 "" H 800 6800 50  0001 C CNN
F 3 "" H 800 6800 50  0001 C CNN
	1    800  6800
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C1
U 1 1 614C4D8E
P 800 3350
F 0 "C1" H 918 3396 50  0000 L CNN
F 1 "10 μF" H 918 3305 50  0000 L CNN
F 2 "" H 838 3200 50  0001 C CNN
F 3 "~" H 800 3350 50  0001 C CNN
	1    800  3350
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C2
U 1 1 614C4E19
P 2100 3300
F 0 "C2" H 2218 3346 50  0000 L CNN
F 1 "1000 μF" H 2218 3255 50  0000 L CNN
F 2 "" H 2138 3150 50  0001 C CNN
F 3 "~" H 2100 3300 50  0001 C CNN
	1    2100 3300
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 614C5C5F
P 6150 2800
F 0 "D3" V 6188 2683 50  0000 R CNN
F 1 "LED (red)" V 6097 2683 50  0000 R CNN
F 2 "" H 6150 2800 50  0001 C CNN
F 3 "~" H 6150 2800 50  0001 C CNN
	1    6150 2800
	0    -1   -1   0   
$EndComp
$Comp
L Device:LED D2
U 1 1 614C5CE4
P 3450 1500
F 0 "D2" V 3488 1383 50  0000 R CNN
F 1 "LED (green)" V 3397 1383 50  0000 R CNN
F 2 "" H 3450 1500 50  0001 C CNN
F 3 "~" H 3450 1500 50  0001 C CNN
	1    3450 1500
	0    -1   -1   0   
$EndComp
$Comp
L Device:R R3
U 1 1 614C6004
P 3450 2700
F 0 "R3" H 3520 2746 50  0000 L CNN
F 1 "220" H 3520 2655 50  0000 L CNN
F 2 "" V 3380 2700 50  0001 C CNN
F 3 "~" H 3450 2700 50  0001 C CNN
	1    3450 2700
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 614C69C1
P 6150 3350
F 0 "R5" H 6220 3396 50  0000 L CNN
F 1 "220" H 6220 3305 50  0000 L CNN
F 2 "" V 6080 3350 50  0001 C CNN
F 3 "~" H 6150 3350 50  0001 C CNN
	1    6150 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R6
U 1 1 614CA35B
P 9250 2000
F 0 "R6" V 9043 2000 50  0000 C CNN
F 1 "680" V 9134 2000 50  0000 C CNN
F 2 "" V 9180 2000 50  0001 C CNN
F 3 "~" H 9250 2000 50  0001 C CNN
	1    9250 2000
	0    1    1    0   
$EndComp
$Comp
L Diode:1N5819 D4
U 1 1 614CA47C
P 10200 2000
F 0 "D4" H 10200 1784 50  0000 C CNN
F 1 "1N5819" H 10200 1875 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 10200 1825 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 10200 2000 50  0001 C CNN
	1    10200 2000
	-1   0    0    1   
$EndComp
Text Notes 9300 1700 0    50   ~ 0
The charging current (1mA) is low enough\nso that one pin can be used directly\nto charge the battery.
Text Notes 2200 800  0    50   ~ 0
The output voltage\nmust be 3.2V.
Text Notes 2250 3800 0    50   ~ 0
C2 should be a big output\ncapacitor for bridging short\ninput voltage drops (bird\nflying over solar panel etc.)
$Comp
L Device:R R1
U 1 1 614ECA84
P 1850 1550
F 0 "R1" H 1920 1596 50  0000 L CNN
F 1 "5.6k" H 1920 1505 50  0000 L CNN
F 2 "" V 1780 1550 50  0001 C CNN
F 3 "~" H 1850 1550 50  0001 C CNN
	1    1850 1550
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 614ECB08
P 1200 5700
F 0 "R2" H 1130 5654 50  0000 R CNN
F 1 "4.7k" H 1130 5745 50  0000 R CNN
F 2 "" V 1130 5700 50  0001 C CNN
F 3 "~" H 1200 5700 50  0001 C CNN
	1    1200 5700
	-1   0    0    1   
$EndComp
$Comp
L Sensor_Temperature:KTY81 TH1
U 1 1 6154F91F
P 5700 3450
F 0 "TH1" H 5797 3496 50  0000 L CNN
F 1 "KTY81" H 5797 3405 50  0000 L CNN
F 2 "" V 5900 3450 50  0001 C CNN
F 3 "http://www.nxp.com/documents/data_sheet/KTY81_SER.pdf" H 5700 3400 50  0001 C CNN
	1    5700 3450
	1    0    0    -1  
$EndComp
$Comp
L Device:R R4
U 1 1 61559D73
P 5700 2950
F 0 "R4" H 5770 2996 50  0000 L CNN
F 1 "R" H 5770 2905 50  0000 L CNN
F 2 "" V 5630 2950 50  0001 C CNN
F 3 "~" H 5700 2950 50  0001 C CNN
	1    5700 2950
	1    0    0    -1  
$EndComp
Text Notes 4700 3650 0    50   ~ 0
TH1 is placed near the\nML2032 cell holder\nso that it can measure\nthe environment\ntemperature.
Wire Wire Line
	800  750  800  900 
Wire Wire Line
	1350 900  950  900 
Connection ~ 800  900 
$Comp
L Diode:1N5819 D1
U 1 1 614C4B5A
P 1150 1250
F 0 "D1" H 1150 1466 50  0000 C CNN
F 1 "1N5819" H 1150 1375 50  0000 C CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 1150 1075 50  0001 C CNN
F 3 "http://www.vishay.com/docs/88525/1n5817.pdf" H 1150 1250 50  0001 C CNN
	1    1150 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1000 1250 950  1250
Wire Wire Line
	950  1250 950  900 
Connection ~ 950  900 
Wire Wire Line
	950  900  800  900 
Wire Wire Line
	3100 900  3100 1250
Wire Wire Line
	10550 2750 10550 6200
Wire Wire Line
	800  6200 800  6800
Wire Wire Line
	800  6200 800  3500
Connection ~ 800  6200
Wire Wire Line
	800  900  800  3200
Wire Wire Line
	1650 1200 1650 2000
$Comp
L Device:R_POT RV1
U 1 1 61617E09
P 1200 5050
F 0 "RV1" H 1130 5096 50  0000 R CNN
F 1 "5k" H 1130 5005 50  0000 R CNN
F 2 "" H 1200 5050 50  0001 C CNN
F 3 "~" H 1200 5050 50  0001 C CNN
	1    1200 5050
	1    0    0    -1  
$EndComp
Text Notes 950  4750 0    50   ~ 0
For fine-adjustment\nof the output voltage\n(3.2V).
$Comp
L Device:Q_NMOS_DGS Q2
U 1 1 615D5233
P 9800 5100
F 0 "Q2" H 10006 5146 50  0000 L CNN
F 1 "BS 170" H 10006 5055 50  0000 L CNN
F 2 "" H 10000 5200 50  0001 C CNN
F 3 "~" H 9800 5100 50  0001 C CNN
	1    9800 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	800  6200 1200 6200
Wire Wire Line
	1200 5850 1200 6200
Connection ~ 1200 6200
Wire Wire Line
	1350 5050 1850 5050
Wire Wire Line
	1850 5050 1850 2000
Connection ~ 1850 2000
Wire Wire Line
	1850 2000 1650 2000
Connection ~ 1850 1250
Wire Wire Line
	1850 1250 1300 1250
Wire Wire Line
	1850 1250 1850 1400
Wire Wire Line
	1850 1700 1850 2000
Wire Wire Line
	1850 1250 2100 1250
Wire Wire Line
	1950 900  2100 900 
Wire Wire Line
	2100 3150 2100 1250
Connection ~ 2100 1250
Wire Wire Line
	2100 1250 2100 900 
Connection ~ 2100 900 
$Comp
L Device:Q_NPN_CBE Q3
U 1 1 61657411
P 8950 5700
F 0 "Q3" H 9140 5746 50  0000 L CNN
F 1 "BC 548C" H 9140 5655 50  0000 L CNN
F 2 "" H 9150 5800 50  0001 C CNN
F 3 "~" H 8950 5700 50  0001 C CNN
	1    8950 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 1250 3450 1250
Wire Wire Line
	4600 1400 4600 1250
Connection ~ 4600 1250
Wire Wire Line
	4600 1250 5700 1250
Wire Wire Line
	4600 2600 4600 3300
Wire Wire Line
	3450 1250 3450 1350
Connection ~ 3450 1250
Wire Wire Line
	3450 1250 4600 1250
Wire Wire Line
	3450 1650 3450 2550
Wire Wire Line
	3450 2850 3450 3300
Wire Wire Line
	3450 3300 4600 3300
Connection ~ 4600 3300
Wire Wire Line
	4600 3300 4600 6200
$Comp
L Device:R R11
U 1 1 61664C04
P 8500 5350
F 0 "R11" H 8300 5300 50  0000 L CNN
F 1 "220k" H 8250 5400 50  0000 L CNN
F 2 "" V 8430 5350 50  0001 C CNN
F 3 "~" H 8500 5350 50  0001 C CNN
	1    8500 5350
	-1   0    0    1   
$EndComp
$Comp
L Device:R R10
U 1 1 61664D4C
P 9000 3900
F 0 "R10" V 8900 3800 50  0000 L CNN
F 1 "100k" V 9100 3800 50  0000 L CNN
F 2 "" V 8930 3900 50  0001 C CNN
F 3 "~" H 9000 3900 50  0001 C CNN
	1    9000 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	5700 3600 5700 6200
Wire Wire Line
	5700 3300 5700 3200
Wire Wire Line
	5700 3200 5450 3200
Connection ~ 5700 3200
Wire Wire Line
	5700 3200 5700 3100
Wire Wire Line
	5700 1250 5700 2800
Wire Wire Line
	6150 3200 6150 2950
Wire Wire Line
	6150 3500 6150 6200
Connection ~ 10550 2400
Wire Wire Line
	10550 2400 10550 2450
Wire Wire Line
	10350 2000 10550 2000
Wire Wire Line
	10550 2000 10550 2400
Wire Wire Line
	6150 2650 6150 1900
Wire Wire Line
	6150 1900 5200 1900
Connection ~ 5700 1250
Connection ~ 9900 2000
Wire Wire Line
	9900 2000 10050 2000
Wire Wire Line
	9900 2000 9900 4900
Wire Wire Line
	9900 5300 9900 6200
Connection ~ 9900 6200
Wire Wire Line
	9900 6200 10550 6200
Wire Wire Line
	9050 5900 9050 6200
Connection ~ 9050 6200
Wire Wire Line
	9050 6200 9900 6200
Wire Wire Line
	9050 5500 9050 5100
Wire Wire Line
	9050 5100 9450 5100
Wire Wire Line
	9150 3900 9450 3900
Wire Wire Line
	9450 3900 9450 5100
Connection ~ 9450 5100
Wire Wire Line
	9450 5100 9600 5100
Wire Wire Line
	8500 5700 8750 5700
Wire Wire Line
	8500 5700 8500 5500
Wire Wire Line
	8850 3900 8750 3900
Wire Wire Line
	8750 3900 8750 1250
Wire Wire Line
	1200 5200 1200 5550
Wire Wire Line
	1200 6200 2100 6200
Wire Wire Line
	2100 3450 2100 6200
Connection ~ 2100 6200
Wire Wire Line
	2100 6200 4600 6200
Connection ~ 4600 6200
Wire Wire Line
	4600 6200 5700 6200
Connection ~ 5700 6200
Wire Wire Line
	5700 6200 6150 6200
Connection ~ 6150 6200
Wire Wire Line
	2100 900  3100 900 
Wire Wire Line
	9400 2000 9900 2000
Wire Wire Line
	5200 2000 9100 2000
$Comp
L Device:R R7
U 1 1 614CA25E
P 9250 2400
F 0 "R7" V 9043 2400 50  0000 C CNN
F 1 "100k" V 9134 2400 50  0000 C CNN
F 2 "" V 9180 2400 50  0001 C CNN
F 3 "~" H 9250 2400 50  0001 C CNN
	1    9250 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	9400 2400 10550 2400
Wire Wire Line
	6400 2400 9100 2400
Wire Wire Line
	7900 5200 7900 4400
Wire Wire Line
	7900 4400 8500 4400
Wire Wire Line
	8500 4400 8500 5200
Wire Wire Line
	7900 5500 7900 6200
Connection ~ 7900 6200
Wire Wire Line
	7900 6200 9050 6200
$Comp
L Device:R R8
U 1 1 6359859F
P 7400 2700
F 0 "R8" H 7470 2746 50  0000 L CNN
F 1 "220" H 7470 2655 50  0000 L CNN
F 2 "" V 7330 2700 50  0001 C CNN
F 3 "~" H 7400 2700 50  0001 C CNN
	1    7400 2700
	1    0    0    -1  
$EndComp
Text Notes 6750 4700 0    50   ~ 0
In case the microcontroller\nhangs itself, it will stop\ncharging the capacitor\nso that Q2 will start\nconducting and thereby\nstopping the charging\nprocess.
$Comp
L MCU_Microchip_ATtiny:ATtiny45-20PU U2
U 1 1 634E1962
P 4600 2000
F 0 "U2" H 4071 2046 50  0000 R CNN
F 1 "ATtiny45-20PU" H 4071 1955 50  0000 R CNN
F 2 "Package_DIP:DIP-8_W7.62mm" H 4600 2000 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/atmel-2586-avr-8-bit-microcontroller-attiny25-attiny45-attiny85_datasheet.pdf" H 4600 2000 50  0001 C CNN
	1    4600 2000
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 634F5CE4
P 7400 3200
F 0 "C3" H 7515 3246 50  0000 L CNN
F 1 "100 nF" H 7515 3155 50  0000 L CNN
F 2 "" H 7438 3050 50  0001 C CNN
F 3 "~" H 7400 3200 50  0001 C CNN
	1    7400 3200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 2850 7400 3000
$Comp
L Device:Q_PNP_CBE Q1
U 1 1 634FC8F3
P 7800 3950
F 0 "Q1" H 7991 3904 50  0000 L CNN
F 1 "BC 558B" H 7991 3995 50  0000 L CNN
F 2 "" H 8000 4050 50  0001 C CNN
F 3 "~" H 7800 3950 50  0001 C CNN
	1    7800 3950
	1    0    0    1   
$EndComp
Wire Wire Line
	7400 3350 7400 3600
Wire Wire Line
	7400 3950 7600 3950
Wire Wire Line
	7900 4150 7900 4400
Connection ~ 7900 4400
$Comp
L Device:R R9
U 1 1 6350D3DD
P 7900 2700
F 0 "R9" H 7970 2746 50  0000 L CNN
F 1 "100" H 7970 2655 50  0000 L CNN
F 2 "" V 7830 2700 50  0001 C CNN
F 3 "~" H 7900 2700 50  0001 C CNN
	1    7900 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 1250 7000 1250
Wire Wire Line
	7900 3750 7900 3600
Wire Wire Line
	7900 1250 7900 2550
Connection ~ 7900 1250
Wire Wire Line
	7900 1250 8750 1250
Connection ~ 7400 3600
Wire Wire Line
	7400 3600 7400 3950
Connection ~ 7900 3600
Wire Wire Line
	7900 3600 7900 2850
$Comp
L Device:D D5
U 1 1 6351B1D3
P 7650 3600
F 0 "D5" H 7650 3383 50  0000 C CNN
F 1 "1N 4148" H 7650 3474 50  0000 C CNN
F 2 "" H 7650 3600 50  0001 C CNN
F 3 "~" H 7650 3600 50  0001 C CNN
	1    7650 3600
	-1   0    0    1   
$EndComp
Wire Wire Line
	7400 3600 7500 3600
Wire Wire Line
	7800 3600 7900 3600
$Comp
L Device:C C4
U 1 1 634F6B16
P 7900 5350
F 0 "C4" H 8015 5396 50  0000 L CNN
F 1 "100 nF" H 8015 5305 50  0000 L CNN
F 2 "" H 7938 5200 50  0001 C CNN
F 3 "~" H 7900 5350 50  0001 C CNN
	1    7900 5350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R?
U 1 1 634FCE09
P 7000 3700
F 0 "R?" H 6850 3650 50  0000 L CNN
F 1 "33k" H 6800 3750 50  0000 L CNN
F 2 "" V 6930 3700 50  0001 C CNN
F 3 "~" H 7000 3700 50  0001 C CNN
	1    7000 3700
	-1   0    0    1   
$EndComp
Wire Wire Line
	6150 6200 6600 6200
Wire Wire Line
	7400 3950 7000 3950
Wire Wire Line
	7000 3950 7000 3850
Connection ~ 7400 3950
Wire Wire Line
	7000 3550 7000 1250
Connection ~ 7000 1250
Wire Wire Line
	7000 1250 7900 1250
Wire Wire Line
	7400 3000 6600 3000
Wire Wire Line
	6600 3000 6600 5000
Connection ~ 7400 3000
Wire Wire Line
	7400 3000 7400 3050
$Comp
L Device:R R?
U 1 1 63515C3F
P 6600 5150
F 0 "R?" H 6670 5196 50  0000 L CNN
F 1 "33k" H 6670 5105 50  0000 L CNN
F 2 "" V 6530 5150 50  0001 C CNN
F 3 "~" H 6600 5150 50  0001 C CNN
	1    6600 5150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 5300 6600 6200
Connection ~ 6600 6200
Wire Wire Line
	6600 6200 7900 6200
Text Notes 4450 7750 0    50   ~ 0
This schematic is licensed under the terms of the\nCERN Open Hardware Licence Version 2 - Strongly Reciprocal\nlicense.
Wire Wire Line
	6400 1700 5200 1700
Wire Wire Line
	6400 1700 6400 2400
Wire Wire Line
	5200 2100 7400 2100
Wire Wire Line
	7400 2100 7400 2550
Wire Wire Line
	5200 1800 5450 1800
Wire Wire Line
	5450 1800 5450 3200
$EndSCHEMATC
