
# ML2032 Chargers

This project is a collection of chargers for ML2032 rechargeable lithium
manganese dioxide button cells and their variants in other sizes (ML2016, ML1220).

WARNING: Charger circuits and software are in development. Do not charge batteries
with these chargers unless you know what you are doing!


## The chargers and their status

### Development status

The development stages for each charger:

- __Development__: The circuit is under development and cannot be used for charging.
- __Alpha stage__: The circuit is able to charge capacitors (supercaps).
  The circuit is on a breadboard.
- __Beta stage__: Charging ML2032 cells is working on a breadboard.
  A defined number of methodic charging tests is passed without errors.
  PCB design starts.
- __Release candidate__: The circuit prototype is on a circuit board.
  A defined number of charging tests is passed without errors.
- __Release__: Small fixes and improvements from the release candidate.


### Simple LM317-based charger

- Directory: simple-charger-lm317
- Status: beta

This is a simple charger designed around the LM317 voltage regualator.
It does CCCV charging with a maximum current of 2mA. There is no undervoltage
protection, so users have to make sure the cell voltage is above 2.0 Volts
before charging a cell with it.

### AVR microcontroller-based charger

- Directory: microcontroller-charger-avr
- Status: development

This is a free software/hardware charger that uses an Atmega8 microcontroller
and is being designed for maximum safety. The software for the microcontroller
shall make sure that a protection for undervoltage, overvoltage and
overtemperature protection is possible.
